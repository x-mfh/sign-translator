import { TranslationAPI } from "../../components/containers/Translation/TranslationAPI"
import { ACTION_TRANSLATION_ATTEMPT_CREATE, translationSuccessCreateAction, translationCreateErrorAction } from "../actions/translationActions"

export const translationMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_TRANSLATION_ATTEMPT_CREATE) {
        TranslationAPI.createTranslation(action.payload)
            .then(() => {
                dispatch(translationSuccessCreateAction())
            })
            .catch(({ message }) => {
                dispatch(translationCreateErrorAction(message))
            })
    }
}