import { StartupAPI } from '../../components/containers/Startup/StartupAPI';
import { ACTION_LOGIN_ATTEMPTING, ACTION_LOGIN_SUCCESS, loginErrorAction, loginSuccessAction } from '../actions/loginActions'
import { sessionSetAction } from '../actions/sessionActions'

export const loginMiddleware = ({dispatch}) => next => action => {
    next(action)

    if(action.type === ACTION_LOGIN_ATTEMPTING) {
        StartupAPI.login(action.payload)
            .then(async profile => {
                if (await profile.length < 1) {
                    StartupAPI.register(action.payload)
                        .then(registered => {
                            dispatch(loginSuccessAction(registered))
                        })
                } else {
                    // return first element since its actual an get request to json-server
                    // and it returns an array
                    dispatch(loginSuccessAction(profile[0]))
                }
            })
            .catch(error => {
                dispatch(loginErrorAction(error.message))
            })
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
        dispatch(sessionSetAction(action.payload))
    }

}