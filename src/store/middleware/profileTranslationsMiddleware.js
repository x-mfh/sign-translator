import { ProfileTranslationsAPI } from "../../components/containers/Profile/ProfileTranslationsAPI"
import { ACTION_PROFILE_TRANSLATIONS_FETCHING, ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_ALL, ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_BY_ID,
     profileTranslationsErrorAction, profileTranslationsSetAction, profileTranslationsSuccessDeleteByIdAction, profileTranslationsSuccessDeleteAllAction,
     profileTranslationsErrorDeleteAction } from "../actions/profileTranslationsActions"

export const profileTranslationsMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if (action.type === ACTION_PROFILE_TRANSLATIONS_FETCHING) {
        ProfileTranslationsAPI.getProfileTranslations(action.payload)
            .then(translations => {
                dispatch(profileTranslationsSetAction(translations))
            })
            .catch(({ message }) => {
                dispatch(profileTranslationsErrorAction(message))
            })
    }

    if (action.type === ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_BY_ID) {
        ProfileTranslationsAPI.deleteProfileTranslationById(action.payload)
            .then(translation => {
                dispatch(profileTranslationsSuccessDeleteByIdAction(translation.id))
            })
            .catch(({ message }) => {
                dispatch(profileTranslationsErrorDeleteAction(message))
            })
    }

    if (action.type === ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_ALL) {
        ProfileTranslationsAPI.deleteProfileTranslations(action.payload)
            .then(() => {
                dispatch(profileTranslationsSuccessDeleteAllAction())
            })
            .catch(({ message }) => {
                dispatch(profileTranslationsErrorDeleteAction(message))
            })
    }
}