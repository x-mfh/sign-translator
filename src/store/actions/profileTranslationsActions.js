export const ACTION_PROFILE_TRANSLATIONS_FETCHING = '[profile-translations] FETCHING'
export const ACTION_PROFILE_TRANSLATIONS_SET = '[profile-translations] SET'
export const ACTION_PROFILE_TRANSLATIONS_ERROR = '[profile-translations] ERROR'
export const ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_BY_ID = '[profile-translations] ATTEMPT DELETE BY ID'
export const ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_BY_ID = '[profile-translations] SUCCESS DELETE BY ID'
export const ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_ALL = '[profile-translations] ATTEMPT DELETE ALL'
export const ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_ALL = '[profile-translations] SUCCESS DELETE ALL'
export const ACTION_PROFILE_TRANSLATIONS_DELETE_ERROR = '[profile-translations] ERROR DELETE'




export const profileTranslationsFetchingAction = userId => ({
    type: ACTION_PROFILE_TRANSLATIONS_FETCHING,
    payload: userId
})

export const profileTranslationsSetAction = translations => ({
    type: ACTION_PROFILE_TRANSLATIONS_SET,
    payload: translations
})

export const profileTranslationsErrorAction = error => ({
    type: ACTION_PROFILE_TRANSLATIONS_ERROR,
    payload: error
})

export const profileTranslationsAttemptDeleteByIdAction = translationId => ({
    type: ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_BY_ID,
    payload: translationId
})

export const profileTranslationsSuccessDeleteByIdAction = translationId => ({
    type: ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_BY_ID,
    payload: translationId
})

export const profileTranslationsAttemptDeleteAllAction = userId => ({
    type: ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_ALL,
    payload: userId
})

export const profileTranslationsSuccessDeleteAllAction = () => ({
    type: ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_ALL
})

export const profileTranslationsErrorDeleteAction = error => ({
    type: ACTION_PROFILE_TRANSLATIONS_DELETE_ERROR,
    payload: error
})