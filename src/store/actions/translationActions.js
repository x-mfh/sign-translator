export const ACTION_TRANSLATION_ATTEMPT_CREATE = '[translation] ATTEMPT CREATE'
export const ACTION_TRANSLATION_SUCCESS_CREATE = '[translation] SUCCESS CREATE'
export const ACTION_TRANSLATION_CREATE_ERROR = '[translation] CREATE_ERROR'

export const translationAttemptCreateAction = translation => ({
    type: ACTION_TRANSLATION_ATTEMPT_CREATE,
    payload: translation
})

export const translationSuccessCreateAction = () => ({
    type: ACTION_TRANSLATION_SUCCESS_CREATE
})

export const translationCreateErrorAction = error => ({
    type: ACTION_TRANSLATION_CREATE_ERROR,
    payload: error
})