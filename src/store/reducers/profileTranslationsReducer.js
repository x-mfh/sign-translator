import { ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_ALL, ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_BY_ID,
         ACTION_PROFILE_TRANSLATIONS_ERROR, ACTION_PROFILE_TRANSLATIONS_FETCHING, 
         ACTION_PROFILE_TRANSLATIONS_SET, ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_BY_ID,
         ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_ALL, ACTION_PROFILE_TRANSLATIONS_DELETE_ERROR } from "../actions/profileTranslationsActions";

const initialState = {
    profileTranslationsError: '',
    profileTranslationsFetching: false,
    profileTranslations: [],
    profileTranslationsDeleteAttempting: false,
    profileTranslationsDeleteSuccess: false,
    profileTranslationsDeleteError: '',
}

export const profileTranslationsReducer = (state = {...initialState}, action) => {
    switch(action.type) {

        case ACTION_PROFILE_TRANSLATIONS_FETCHING:
            return {
                ...state,
                profileTranslationsError: '',
                profileTranslationsFetching: true,
            }
        case ACTION_PROFILE_TRANSLATIONS_SET:
            return {
                profileTranslationsError: '',
                profileTranslationsFetching: false,
                profileTranslations: [...action.payload]
            }
        case ACTION_PROFILE_TRANSLATIONS_ERROR:
            return {
                ...state,
                profileTranslationsError: action.payload,
                profileTranslationsFetching: false
            }
        case ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_BY_ID:
            return {
                ...state,
                profileTranslationsDeleteAttempting: true,
                profileTranslationsDeleteSuccess: false
            }
        case ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_BY_ID:
            return {
                ...state,
                profileTranslations: state.profileTranslations.filter(translation => translation.id !== action.payload),
                profileTranslationsDeleteSuccess: true,
                profileTranslationsDeleteAttempting: false
            }
        case ACTION_PROFILE_TRANSLATIONS_ATTEMPT_DELETE_ALL:
            return {
                ...state,
                profileTranslationsDeleteAttempting: true,
                profileTranslationsDeleteSuccess: false
            }
        case ACTION_PROFILE_TRANSLATIONS_SUCCESS_DELETE_ALL:
            return {
                ...state,
                // better to filter ??
                profileTranslations: [],
                profileTranslationsDeleteSuccess: true,
                profileTranslationsDeleteAttempting: false
            }
        case ACTION_PROFILE_TRANSLATIONS_DELETE_ERROR:
            return {
                ...state,
                profileTranslationsDeleteAttempting: false,
                profileTranslationsDeleteError: action.payload
            }
        default:
            return state

    }
}