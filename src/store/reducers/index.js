import { combineReducers } from 'redux'
import { loginReducer } from './loginReducer'
import { sessionReducer } from './sessionReducer'
import { translationReducer } from './translationReducer'
import { profileTranslationsReducer } from './profileTranslationsReducer'

const appReducer = combineReducers({
    loginReducer,
    sessionReducer,
    translationReducer,
    profileTranslationsReducer
})

export default appReducer