import { ACTION_TRANSLATION_CREATE_ERROR, ACTION_TRANSLATION_ATTEMPT_CREATE, ACTION_TRANSLATION_SUCCESS_CREATE } from "../actions/translationActions";

const initialState = {
    translationCreateAttempting: false,
    translationCreateError: '',
    translationCreateSuccess: false
}

export const translationReducer = (state = { ...initialState }, action) => {
    switch (action.type) {

        case ACTION_TRANSLATION_ATTEMPT_CREATE:
            return {
                ...state,
                translationCreateAttempting: true,
                translationCreateError: '',
                translationCreateSuccess: false
            }
        case ACTION_TRANSLATION_SUCCESS_CREATE:
            return {
                ...initialState,
                translationCreateSuccess: true
            }
        case ACTION_TRANSLATION_CREATE_ERROR:
            return {
                ...state,
                translationCreateError: action.payload
            }
        default:
            return {...initialState}

    }
}