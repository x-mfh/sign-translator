export const filterOutNumbersAndSymbol = sentence => {
    return sentence.split('').map(letter => {
        if (letter.match(/^[a-zA-Z]*$/)) {
            return letter
        } else if (letter === " ") {
            return " "
        } else {
            return ""
        }
    }).join('')
}