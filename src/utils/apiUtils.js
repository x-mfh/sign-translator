export const handleFirstResponse = async response => {
    if (!response.ok) {
        const { error = 'Unknown error occurred.' } = await response.json();
        throw new Error(error)
    }
    return response.json();
}