import AppHeader from './components/shared/AppHeader';
import './App.css';
import Startup from './components/containers/Startup/Startup'
import Translation from './components/containers/Translation/Translation'
import Profile from './components/containers/Profile/Profile'
import NotFound from './components/containers/NotFound/NotFound';

import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import RoutePrivate from './components/hoc/RoutePrivate';


function App() {
    return (
        <Router>
            <div className="App">
                <AppHeader />
                <Switch>
                    <RoutePrivate path="/translation" component={ Translation } />
                    <RoutePrivate path="/profile" component={ Profile } />
                    <Route path="/" exact component={ Startup } />
                    <Route path="*" component={ NotFound } />
                </Switch>
            </div>
        </Router>
    );
}

export default App;
