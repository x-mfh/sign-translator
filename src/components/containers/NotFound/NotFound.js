import { Link } from "react-router-dom";
import { yellowBgColor } from "../../../consts/colorPalette";
import AppBoxContainer from "../../hoc/AppBoxContainer";
import AppContainer from "../../hoc/AppContainer";

function NotFound() {
    return (
        <main>
            <AppContainer bgColor={yellowBgColor}>
                <AppBoxContainer>
                    <h2>Hey, you seem lost.</h2>
                    <p>This page does not exist</p>
                    <Link to="/">Take me home</Link>
                </AppBoxContainer>
            </AppContainer>
        </main>
    );
}

export default NotFound;