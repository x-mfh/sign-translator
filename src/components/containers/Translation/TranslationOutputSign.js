import React from 'react'
import styles from './TranslationOutputSign.module.css'

const TranslationOutputSign = ({ textToTranslate }) => {
    return (
        <>
            { textToTranslate &&
                textToTranslate.split('').map((letter, idx) => {
                    if (letter.match(/^[a-zA-Z]*$/)) {
                        return <img key={`sign-${idx}`} src={process.env.PUBLIC_URL + `/individial_signs/${letter}.png`} 
                                    alt={`${letter} sign`} className={ styles.tosSignImage } /> 
                    } else if (letter === " ") {
                        return <div key={`sign-${idx}`} className={ styles.tosWhitespaceFiller }></div>
                    } else {
                        return ""
                    }
                })
            }
        </>
        
    )
}

export default TranslationOutputSign;