import { useState } from 'react';
import TranslationForm from "./TranslationForm";
import TranslationOutput from "./TranslationOutput";
import AppContainer from "../../hoc/AppContainer";
import AppBoxContainer from "../../hoc/AppBoxContainer";
import { yellowBgColor } from "../../../consts/colorPalette";

const Translation = () => {

    const [ text, setText ] = useState('')

    const handleTextChanged = input => {
        setText(input);
    }

    return (
        <main>
            <AppContainer bgColor={ yellowBgColor }>
                <TranslationForm translateText={ handleTextChanged } />
            </AppContainer>
            <AppContainer>
                <AppBoxContainer>
                    <h1 className="heading">Translation</h1>    
                    <TranslationOutput textToTranslate={ text } />
                </AppBoxContainer>
            </AppContainer>
        </main>
    )
}

export default Translation;