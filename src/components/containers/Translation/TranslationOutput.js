import TranslationOutputSign from './TranslationOutputSign'

const TranslationOutput = ({ textToTranslate }) => {
    return (
        <>
            <TranslationOutputSign textToTranslate={textToTranslate}/>
        </>
    )
}

export default TranslationOutput;