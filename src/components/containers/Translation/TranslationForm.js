import { useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { filterOutNumbersAndSymbol } from "../../../utils/inputFilterUtil";
import AppFormInline from "../../shared/AppFormInline";
import styles from './TranslationForm.module.css'
import { translationAttemptCreateAction } from "../../../store/actions/translationActions"


const TranslationForm = ({ translateText }) => {

    const { id: userId } = useSelector(state => state.sessionReducer)
    const { translationCreateError, translationCreateAttempting, translationCreateSuccess } = useSelector(state => state.translationReducer);
    const dispatch = useDispatch()

    const [ translation, setTranslation ] = useState({
        text: '',
        deleted: false,
        userId
    })

    function onInputChange(event) {
        setTranslation({
            ...translation,
            [event.target.id]: filterOutNumbersAndSymbol(event.target.value.trim())
        });
    }

    function onFormSubmit(event) {
        event.preventDefault();
        translateText(translation.text);
        dispatch(translationAttemptCreateAction(translation))
    }

    return (
        <>
            <AppFormInline 
                handleFormSubmit={ onFormSubmit } 
                handleInputChange={ onInputChange } 
                inputIdentifier="text" 
                inputPlaceholder="What would you like to translate" 
            />
            <p className={ styles.inputDescription }>Only accepts letters from a-z and spaces. No numbers or symbols are supported.</p>

            { translationCreateAttempting &&
                <p className="displayInfo display-mt">Trying to save your translation...</p>
            }

            { translationCreateSuccess &&
                <p className="displaySuccess display-mt">Successfully saved your translation</p>
            }

            { translationCreateError &&
                <div className="displayError display-mt" role="alert">
                    <h4>Unsuccessful</h4>
                    <p>{ translationCreateError }</p>
                </div>
            }
        </>
    )
}

export default TranslationForm;