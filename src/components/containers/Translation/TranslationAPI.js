import { handleFirstResponse } from "../../../utils/apiUtils"

export const TranslationAPI = {
    createTranslation(translation) {
        const newTranslation = {
            ...translation,
            createdAt: Date.now()
        }
        return fetch(`http://localhost:8000/translations`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newTranslation)
        })
        .then(handleFirstResponse)
    }
}