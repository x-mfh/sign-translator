import { handleFirstResponse } from "../../../utils/apiUtils"

export const StartupAPI = {
    // Fake login
    login(credentials) {
        return fetch(`http://localhost:8000/users?username=${credentials.username}`)
        .then(handleFirstResponse)
    },
    register(credentials) {
        const newUser = {
            ...credentials,
            createdAt: Date.now()
        }
        return fetch(`http://localhost:8000/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newUser)
        })
        .then(handleFirstResponse)
    }
    
}