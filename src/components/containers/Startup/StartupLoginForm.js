import { useState } from "react";
import AppFormInline from "../../shared/AppFormInline";

import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loginAttemptAction } from '../../../store/actions/loginActions';

const StartupLoginForm = () => {

    const disPatch = useDispatch();
    const { loginError, loginAttempting } = useSelector(state => state.loginReducer);
    const { loggedIn } = useSelector(state => state.sessionReducer);

    const [credentials, setCredentials ] = useState({
        username:''
    });

    function onInputChange(event) {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        });
    }

    function onFormSubmit(event) {
        event.preventDefault();
        const credentialsLowercaseUsername = { 
            ...credentials, 
            username: credentials.username.toLowerCase()
        }
        disPatch(loginAttemptAction(credentialsLowercaseUsername))
    }

    return (
        <>
            { loggedIn && <Redirect to="/translation" /> }
            { !loggedIn && 
                <AppFormInline 
                    handleFormSubmit={ onFormSubmit } 
                    handleInputChange={ onInputChange } 
                    inputIdentifier="username" 
                    inputPlaceholder="What's your name?" 
                />
            }

            { loginAttempting &&
                <p className="displayInfo display-mt">Trying to login...</p>
            }

            { loginError &&
                <div className="displayError display-mt" role="alert">
                    <h4>Unsuccessful</h4>
                    <p>{ loginError }</p>
                </div>
            }
        </>
    )
}

export default StartupLoginForm;