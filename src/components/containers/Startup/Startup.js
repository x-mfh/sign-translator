import StartupLoginForm from "./StartupLoginForm";
import LogoHello from '../../shared/img/Logo-Hello.png'
import Splash from '../../shared/img/Splash.svg'
import './Startup.css'
import AppBoxContainer from "../../hoc/AppBoxContainer";
import AppContainer from "../../hoc/AppContainer";
import { yellowBgColor } from "../../../consts/colorPalette";

const Startup = () => {
    return (
        <main>
            <AppContainer bgColor={yellowBgColor}>
                <div className="startup">
                    <img className="startup__logoSplash" src={Splash} alt="splash" />    
                    <img className="startup__logo" src={LogoHello} alt="Hello robot" />
                    <div className="startup-hero">
                        <h1 className="heading">Lost in translation</h1>
                        <p>Get started</p>
                    </div>
                </div>
            </AppContainer>
            <div className="startup-login-wrapper">
                <AppBoxContainer>
                    <StartupLoginForm />
                </AppBoxContainer>
            </div>
        </main>
    )
}

export default Startup;