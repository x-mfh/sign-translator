import { useEffect } from "react";
import ProfileTranslationListItem from "./ProfileTranslationsListItem";
import styles from './ProfileTranslationsList.module.css'
import { useDispatch, useSelector } from "react-redux";
import { profileTranslationsFetchingAction, profileTranslationsAttemptDeleteAllAction } from "../../../store/actions/profileTranslationsActions";

const ProfileTranslationList = () => {

    const { 
        profileTranslations, 
        profileTranslationsFetching, 
        profileTranslationsError,
        profileTranslationsDeleteAttempting,
        profileTranslationsDeleteSuccess,
        profileTranslationsDeleteError
    } = useSelector(state => state.profileTranslationsReducer)
    const session = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(profileTranslationsFetchingAction(session.id))
    }, [dispatch, session])

    const handleDeleteAllTranslations =() => {
        // UNCOMMENT WHEN API CALL IS FIXED :-(
        // dispatch(profileTranslationsAttemptDeleteAllAction(session.id))
    }

    return (
        <>
            <div className={styles.ptlHeader}>
                <h2 className="heading">Lastest translations</h2>
                { profileTranslationsDeleteAttempting && 
                    <p className="displayInfo">Deleting translation(s)...</p> 
                }
                { profileTranslationsDeleteSuccess && 
                    <p className="displaySuccess">Deleted translation(s)...</p> 
                }
                { profileTranslationsDeleteError && 
                    <p className="displayError">{profileTranslationsDeleteError}</p> 
                }
                <button className={styles.ptlHeaderDeleteAllBtn} onClick={ handleDeleteAllTranslations}>
                    <span className="material-icons">delete_forever</span>
                    Clear all translations 
                </button>
            </div>
            
            { profileTranslationsFetching && 
                <p className="displayInfo">Loading translations...</p> 
            }
            { profileTranslations.map(translation => { 
                return <ProfileTranslationListItem key={`trans-${translation.id}`} translationId={translation.id} translationText={translation.text} />
                })
            }
            { profileTranslationsError && 
                <p className="displayError">{profileTranslationsError}</p>
            }
        </>
    )
}

export default ProfileTranslationList;