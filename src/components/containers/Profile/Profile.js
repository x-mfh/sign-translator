import './Profile.css';
import AppContainer from "../../hoc/AppContainer";
import AppBoxContainer from '../../hoc/AppBoxContainer';
import { yellowBgColor } from "../../../consts/colorPalette";
import ProfileTranslationList from "./ProfileTranslationsList";
import { useSelector } from 'react-redux'


const Profile = () => {
    const { username = '' } = useSelector(state => state.sessionReducer)

    return (
        <main>
            <AppContainer bgColor={ yellowBgColor }>
                <div className="profile-hero">
                    <h1 className="heading">Hi, {username}</h1>
                    <p>Welcome to your profile</p>
                </div>
            </AppContainer>
            <AppContainer>
                <AppBoxContainer>
                    <ProfileTranslationList />
                </AppBoxContainer>
            </AppContainer>
        </main>
    )
}

export default Profile;