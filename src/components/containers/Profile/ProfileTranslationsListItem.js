import styles from './ProfileTranslationsListItem.module.css'
import { useDispatch } from "react-redux";
import { profileTranslationsAttemptDeleteByIdAction } from "../../../store/actions/profileTranslationsActions";

const ProfileTranslationListItem = ({ translationId, translationText }) => {

    const dispatch = useDispatch()
    const handleDeleteTranslation =() => {
        dispatch(profileTranslationsAttemptDeleteByIdAction(translationId))
    }

    return (
        <div className={styles.ptlItem}>
            <span>{ translationText }</span>
            <button className={ styles.ptlItemDelete + " material-icons"} onClick={handleDeleteTranslation} >delete_forever</button>
        </div>
    )
}

export default ProfileTranslationListItem;