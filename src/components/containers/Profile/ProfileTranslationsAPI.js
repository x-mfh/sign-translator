import { handleFirstResponse } from "../../../utils/apiUtils"

export const ProfileTranslationsAPI = {
    getProfileTranslations(userId) {
        return fetch(`http://localhost:8000/translations?userId=${userId}&deleted=false&_limit=10&_sort=createdAt&_order=desc`)
        .then(handleFirstResponse)
    },
    // NOT WORKING ATM, IS IT POSSIBLE TO PATCH MULTIPLE ENTITIES
    deleteProfileTranslations(userId) {
        return fetch(`http://localhost:8000/users/${userId}/translations`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ deleted: true })
        })
        .then(handleFirstResponse)
    },
    deleteProfileTranslationById(translationId) {
        return fetch(`http://localhost:8000/translations/${translationId}`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ deleted: true })
        })
        .then(handleFirstResponse)
    }
}