import './AppContainer.css'

const AppContainer = ({ children, bgColor }) => {

    const styles = {
        backgroundColor: bgColor ? bgColor : "white"
    }

    return (
        <div style={styles} className="appContainer">{ children }</div>
    )
}

export default AppContainer;