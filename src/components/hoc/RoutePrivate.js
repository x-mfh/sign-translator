import { useSelector } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'
const RoutePrivate = props => {

    const { loggedIn } = useSelector(state => state.sessionReducer)

    if (!loggedIn) {
        return <Redirect to="/" />
    } else {
        return <Route {...props} />
    }

}
export default RoutePrivate