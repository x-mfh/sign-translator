import './AppBoxContainer.css'

const AppBoxContainer = ({ children }) => {
    return (
        <div className="appBoxContainer">{ children }</div>
    )
}

export default AppBoxContainer;