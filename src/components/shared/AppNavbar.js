import './AppNavbar.css'
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux'
import { sessionLogoutAction } from '../../store/actions/sessionActions'




const AppNavbar = () => {
    const { username } = useSelector(state => state.sessionReducer)
    const dispatch = useDispatch()

    const onLogoutClick = () => {
        dispatch(sessionLogoutAction())
    }

    return (
        <>
            { username && 
                <nav className="header-nav">
                    <NavLink className="header-nav__a" to="/translation">Translation</NavLink>
                    <NavLink className="header-nav__a" to="/profile">Profile</NavLink>
                    <button className="header-nav__logoutBtn" onClick={onLogoutClick}>Logout</button>
                </nav>
            }
        </>
    )
}

export default AppNavbar;