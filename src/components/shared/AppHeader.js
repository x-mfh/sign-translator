import './AppHeader.css'
import { Link } from "react-router-dom";
import AppNavbar from "./AppNavbar";

const AppHeader = () => {
    return (
        <header className="header">
            <Link className="header__logo" to="/">
                <h1 className="heading">Lost in Translation</h1>
            </Link>
            <AppNavbar />
        </header>
    )
}

export default AppHeader;