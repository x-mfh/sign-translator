import styles from './AppFormInline.module.css'

const AppFormInline = ({
    handleFormSubmit,
    handleInputChange,
    inputIdentifier,
    inputPlaceholder
}) => {
    return (
        <form className={styles.AppFormInline} onSubmit={ handleFormSubmit }>
            <span className={ styles.AppFormInlineInputIconBeginning + " material-icons"}>keyboard</span>
            <input className={ styles.AppFormInlineInput } id={ inputIdentifier } name={ inputIdentifier } type="text" onChange={ handleInputChange } placeholder={ inputPlaceholder } />
            <button onClick={ handleFormSubmit } className={ styles.AppFormInlineInputIconEnd + " material-icons"}>arrow_forward</button>
        </form>
    )
}

export default AppFormInline;